<?php
// Remarks
// - pcntl_fork();  not available! pff  // doc multi thread http://php.net/manual/en/function.pcntl-fork.php

function validOwner($owner) {
  if (!isset($owner) || empty($owner)) {
    throw new Exception("'owner' parameter expected");
  }
}

function validLimit($limit) {
  $exMsg = "invalid 'limit' parameter: expect int from 1 to 500";
  if (!is_int($limit) || $limit < 1 || $limit > 500) {
    throw new Exception($exMsg);
  }
}


function getGeokretsByUsername($username, $limit) {
  $gkMapUrl = "https://api.geokretymap.org/geojson?latTL=89.456&lonTL=508.359&latBR=-80.058&lonBR=-553.359&limit=$limit&json=1&daysFrom=0&daysTo=-1&ownername=$username";
  $gkmJson = file_get_contents($gkMapUrl);
  $gkm = json_decode($gkmJson);
  // TOO VERBOSE // if ($verbose) echo $gkmJson;

   $geokretsIds = array();
   $count = count($gkm->features);
   for ($i=0;$i<$count;$i++) {
      array_push($geokretsIds, $gkm->features[$i]->properties->gkid);
   }
   return $geokretsIds;
}


function displayGK($geokretId) {
  echo "display Geokret $geokretId<br/>";

  $url = "https://api.geokretymap.org/gk/$geokretId/details";
  $xml = simplexml_load_file($url);

  //~ xml to var mapping
  $gkName = utf8_encode($xml->geokrety->geokret->name);
  $gkDesc = $xml->geokrety->geokret->description;
  $gkPlaces = $xml->geokrety->geokret->places;
  $gkState = $xml->geokrety->geokret->state;
  $gkDist = $xml->geokrety->geokret->distancetraveled;
  $gkDistUnit = $xml->geokrety->geokret->distancetraveled['unit'];
  $gkNbMove = $xml->geokrety->geokret->moves->children()->count();

  echo "name: $gkName<br/>";
  echo "desc: <pre>$gkDesc</pre><br/>";
  echo "travel: $gkDist $gkDistUnit ($gkNbMove moves, $gkPlaces places, $gkState states)<br/>";
}


function getGeokretsDetails($geokrets) {
  $gkd = array(); // geokrets details
  $nb = count($geokrets);
  for ($j=0;$j<$nb;$j++) {
    $gkId = $geokrets[$j];
    $url = "https://api.geokretymap.org/gk/$gkId/details";
    $xml = simplexml_load_file($url);
    array_push($gkd, $xml->geokrety->geokret);
  }
  return $gkd;
}

function orderKM($a, $b) {
  $adt = intval($a->distancetraveled);
  $bdt = intval($b->distancetraveled);
  if ($adt == $bdt) {
      return 0;
  }
  $rez = ($adt > $bdt) ? -1 : 1;
  return $rez;
}

function sortGeokrets(&$gk, $order) {
  if (strcmp($order, "KM") !== 0) {
    throw new Exception("order '$order' not yet implemented");
  }
  // Trie le tableau avec la fonction orderKM
  usort($gk, "orderKM");
}

function deltaTime($a, $b) {
  $delta = date_diff($a, $b);
  $rez = "";
  if ($delta->h > 0) {
        $rez = "$rez" . $delta->h . "h ";
  }
  if ($delta->i > 0) {
        $rez = "$rez" . $delta->i . "min ";
  }
  if ($delta->s >= 0) {
        $rez = "$rez" . $delta->s . "s";
  }
  return $rez;
}


?>
