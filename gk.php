<?php
/**
  * parameters
  *   limit: integer between 1 and 500 (default=500)
  *   owner: username *required*
  *
  * doc: cf. https://bitbucket.org/bvandeputte/gkwidget/wiki/gkwidget
  */
include 'gklib.php';

$limit=500;
$owner=$_GET['owner'];

$order="KM";
$result=5;
$verbose=true;
$generationTime = true;

try {
  $time_start = new DateTime();

  if (isset($_GET['limit'])) {
	$limit = intval($_GET['limit']);
  }


  validOwner($owner);
  validLimit($limit);

  //~ get owner Geokrets
  $gkrets = getGeokretsByUsername($owner, $limit);
  $time_geokrets = new DateTime();

  $nbGK = count($gkrets);
  $gkretsDetails = getGeokretsDetails($gkrets);
  $time_details = new DateTime();

  sortGeokrets($gkretsDetails, $order);
  $time_sort = new DateTime();

  if ($verbose) echo "$owner : $nbGK geokrets<br/>";

  if (false && $verbose && $nbGK > 0) {
    $firstGK = $gkrets[0];
    if ($verbose) echo "first gk: $firstGK<br/>";
    if (isset($firstGK)) {
      displayGK($firstGK);
    }
    echo "<hr/>";
  }
  
  echo "<br/>Geokret by distance:<br/>";
  for ($r = 0 ; $r < $result && $r < $nbGK; $r++) {
    $gkId   = $gkretsDetails[$r]['id'];
    $gkGk   = sprintf("GK%04X",$gkId);
    $gkName = $gkretsDetails[$r]->name;
    $gkDt   = $gkretsDetails[$r]->distancetraveled;
    $gkUnit = $gkretsDetails[$r]->distancetraveled['unit'];
    echo " -  #$gkGk $gkName $gkDt $gkUnit <br/>";
  }

  if ($generationTime) {
   $time_last = new DateTime();
   $time0 = deltaTime($time_geokrets, $time_start);
   $time1 = deltaTime($time_details, $time_geokrets);
   $time2 = deltaTime($time_sort, $time_details);
   $time3 = deltaTime($time_last, $time_sort);
   $time9 = deltaTime($time_last, $time_start);
   echo "<br/>(gen $time9) list:$time0 item:$time1 /\:$time2 out:$time3<br/>";
  }

} catch (Exception $e) {
    echo 'Exception: ',  $e->getMessage(), "\n";
}

?>
