<?php
/**
  * REQUIREMENT: php gd  / sudo apt-get install php5-gd
  * 
  * parameters
  *   limit: integer between 1 and 500 (default=500)
  *   owner: username *required*
  *
  * doc: cf. https://bitbucket.org/bvandeputte/gkwidget/wiki/gkwidget
  */
include 'gklib.php';
include 'gkgdlib.php';

$limit=500;
$owner=$_GET['owner'];

$order="KM";
$result=5;
$verbose=false;
$intro=false;
$generationTime = isset($_GET['genTime']);


$msgImg = array();

try {
  $time_start = new DateTime();

  if (isset($_GET['limit'])) {
	$limit = intval($_GET['limit']);
  }
  if (isset($_GET['nbresult'])) {
	$result = intval($_GET['nbresult']);
	if ($result > 20) {
		$result = 5;
	}
  }
  if (isset($_GET['intro'])) {
        $intro = true;
  }



  validOwner($owner);
  validLimit($limit);

  //~ get owner Geokrets
  $gkrets = getGeokretsByUsername($owner, $limit);
  $time_geokrets = new DateTime();

  $nbGK = count($gkrets);
  if ($nbGK == 0) {
	throw new Exception("no geokret for $owner");
  }
  $gkretsDetails = getGeokretsDetails($gkrets);
  $time_details = new DateTime();

  sortGeokrets($gkretsDetails, $order);
  $time_sort = new DateTime();

  if ($intro) {
    array_push($msgImg, "$owner : $nbGK geokrets order by $order");
  }

  for ($r = 0 ; $r < $result && $r < $nbGK; $r++) {
    $gkId   = $gkretsDetails[$r]['id'];
    $gkGk   = sprintf("GK%04X",$gkId);
    $gkName = $gkretsDetails[$r]->name;
    $gkDt   = $gkretsDetails[$r]->distancetraveled;
    $gkUnit = $gkretsDetails[$r]->distancetraveled['unit'];

//    $gkElem = "$gkDt $gkUnit  $gkName";
    $gkElem = sprintf("%' 6d $gkUnit  $gkName", $gkDt);
    array_push($msgImg, $gkElem);
  }

  if ($generationTime) {
   $time_last = new DateTime();
   $time0 = deltaTime($time_geokrets, $time_start);
   $time1 = deltaTime($time_details, $time_geokrets);
   $time2 = deltaTime($time_sort, $time_details);
   $time3 = deltaTime($time_last, $time_sort);
   $time9 = deltaTime($time_last, $time_start);
   $generationTime = "(gen $time9) l:$time0 i:$time1 /\:$time2 o:$time3";
   array_push($msgImg, $generationTime);
  }
  generateImage($msgImg);

} catch (Exception $e) {
   $exMsg = "Exception: ". $e->getMessage(). "\n";
   array_push($msgImg, $exMsg);
   generateImage($msgImg);
}

?>
